
provider "aws" {
  region = "us-east-1" 
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}


resource "aws_subnet" "public" {
  vpc_id            = aws_vpc.main.id
  cidr_block         = "10.0.1.0/24"
  availability_zone = "us-east-1a"


  map_public_ip_on_launch = true
}


resource "aws_security_group" "ssh" {
  name = "ssh-access"
  description = "Allow SSH access"

  ingress {
    from_port = 22
    to_port   = 22
    protocol = "tcp"

    cidr_blocks = ["0.0.0.0/0"] 
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }
}

