node default {
  # Update package list
  package { 'yum-utils' } -> ensure => present

  # Install Apache web server
  package { 'httpd' } -> ensure => present

  # Start and enable Apache service
  service { 'httpd' } {
    ensure => running
    enable => true
  }
}
